Syntax
======

Clinical work flow
------------------

The clinical workflow is a series of events. In lympha this is formulated as statements divided by ``->``. A series of statements ends with semicolon as follows:
	.. code-block:: none
		
		statement -> statement -> statement ;
		
The name of the statement consists of one word, made of letters a-z, A-Z, digits 0-9, full stop (``.``), question mark, hyphen (``-``) and underscore (``_``). 

Some events are procedures that should be executed. In lympha, these events are called *procedures*. Some events do not directly involve the patient, but data derived from diagnostics. Those events are called *factors*. Hence there are two types of statements in lympha: procedures and factors. 


Differences between procedrues and factors
------------------------------------------

The name of a procedure always ends with a full stop (``.``) meanwhile, factors always end with a question mark (``?``).

Depending on if a procedure should be executed or not, it has a *value* of ``1`` or ``0``. By default, all procedures have the value set to ``1``.

The value of a factor can be declared in three ways:
	- Make it equal to a real number (ℝ). Example:
		.. code-block:: none
			
			body_temperature_in_C? = 35.8 ;

	- The value of the factor can also be dependent upon other factors. These dependent factors are called *binary factors*. The binary factor name in question is followed by one equal sign (``=``). Then follows a tipping point. After that, there is an operator. Below are valid operators listed:
		.. csv-table:: Valid rational operators
			:widths: 15, 10
			:header: "relational operator", "read as"

			"``==``", "if and only if (≡)"
			"``>``", "greater than"
			"``>=``", "greater than or equal (≥)"
			"``<``", "lesser than"
			"``<=``", "less than or equal (≤)"
			"``!=``", "not equal to (≠)"

		After the operator follows either the name of a non-binary factor or a series of binary factors.
		
			* In the case where the binary factor depends on a non-binary factor:
				.. code-block:: none
			
					binary-factor = tipping-point operator non-binary-factor ;
			
				Example:
					.. code-block:: none
				
						hypothermia? = 36 < body_temperature_in_C? ;
			
			* In the case where the binary factor depends on a non-binary factor:
				.. code-block:: none
				
					binary-factor = tipping-point operator |{non-binary-factors}|
			
				Example:			
					.. code-block:: none
				
						resp? = 1 < |{tachypnea?, pCO2_blood_less4.3?}| ;
						SIRS? = 2 =< |{ hypothermia?, temp_above_38?, HR_greater_90?, resp?, wbc_less4000?, wbc_greater12000?  }| ;


In order for a procedure or a binary-factor to get the value ``1``, at least one parent-statement must have the value ``1``. 
