.. toctree::
	:caption: Table of Contents
	:name: mastertoc
	:hidden: 
	:maxdepth: 1

	Introduction<index>
	syntax
	tutorials
